import CartParser from './CartParser.js';

let parser, validate, calcTotal, parseLine, parse;

beforeEach(() => {
	parser = new CartParser();
	validate = parser.validate.bind(parser);
	calcTotal = parser.calcTotal.bind(parser);
	parseLine = parser.parseLine.bind(parser);
	parse = parser.parse.bind(parser);
});

describe('CartParser - unit tests', () => {
	it('should return no errors when header and bodyLines are valid', () => {
		expect(validate('Product name, Price, Quantity\nItem, 1, 1')).toEqual([]);
	});

	it('should return an error of type "header" when any of headers name is wrongly spelled', () => {
		expect(validate('Product name, Price, quantity')).toContainEqual(
			expect.objectContaining({ type: 'header' }));
	});

	it('should return an error of type "row" when numbers of rows in bodyLine less than 3', () => {
		expect(validate('Product name, Price, Quantity\nItem, 1')).toContainEqual(
			expect.objectContaining({ type: 'row' }));
	});

	it('should return an error of type "cell" when the first cell in bodyLine is an empty string', () => {
		expect(validate('Product name, Price, Quantity\n, 1, 1')).toContainEqual(
			expect.objectContaining({ type: 'cell' }));
	});

	it('should return an error of type "cell" when the second or third cell in bodyLine is less than 0', () => {
		expect(validate('Product name, Price, Quantity\nItem, -1, 1')).toContainEqual(
			expect.objectContaining({ type: 'cell' }));
	});

	it('should return an error of type "cell" when the second or third cell in bodyLine is NaN', () => {
		expect(validate('Product name, Price, Quantity\nItem, NaN, 1')).toContainEqual(
			expect.objectContaining({ type: 'cell' }));
	});

	it('should return an error of type "cell" when the second or third cell in bodyLine is not of type number', () => {
		expect(validate('Product name, Price, Quantity\nItem, string, 1')).toContainEqual(
			expect.objectContaining({ type: 'cell' }));
	});

	it('should return an object with exactly 4 keys', () => {
		const keysLength = Object.keys(parseLine('Item, 1, 1')).length;
		expect(keysLength).toBe(4);
	});

	it('should return an object which contains specific keys: id, name, price, and quantity', () => {
		expect(parseLine('Item, 1, 1')).toHaveProperty('id', 'name', 'price', 'quantity');
	});

	it('should return a number which is equal or greater than 0', () => {
		const items = [
			{
				"id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
				"name": "Mollis consequat",
				"price": 9,
				"quantity": 2
			}
		];
		expect(calcTotal(items)).toBeGreaterThanOrEqual(0);
	});
});

describe('CartParser - integration test', () => {
	it('should return expected result defined in expectedOutput', () => {
		const csv = './samples/cart.csv';
		// Ids are omitted as they generated randomly
		const expectedOutput = {
			items: [
			  {
				name: "Mollis consequat",
				price: 9,
				quantity: 2
			  },
			  {
				name: "Tvoluptatem",
				price: 10.32,
				quantity: 1
			  },
			  {
				name: "Scelerisque lacinia",
				price: 18.9,
				quantity: 1
			  },
			  {
				name: "Consectetur adipiscing",
				price: 28.72,
				quantity: 10
			  },
			  {
				name: "Condimentum aliquet",
				price: 13.9,
				quantity: 1
			  }
			],
			total: 348.32
		  };
		expect(parse(csv)).toMatchObject(expectedOutput);
	});
});